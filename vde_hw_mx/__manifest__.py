# -*- coding: utf-8 -*-
# ##############################################################################
#

#    OpenERP, Open Source Management Solution
#    Copyright (C) 2012-Today Acespritech Solutions Pvt Ltd
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'VDE HW MX',
    'version': '1.0',
    'depends': [
            'base'
    ],
    'license': 'Other proprietary',
    'price': 999.0,
    'category' : 'Sales',
    'currency': 'EUR',
    'summary': """Huawei Ecommerce integration platform CFDI 3.3""",
    'description': "",
    'author': 'VDE Suite',
    'website': 'https://www.vde-suite.com',
    'support': '911@vde-suite.com.com',
    'images': [],
#    'live_test_url': 'https://vde-suite.com',

    'data':[
#        'security/ir.model.access.csv',
#        'wizard/sale_reservation_wiz_view.xml',
#        'wizard/sale_create_po_view.xml',
#        'wizard/sale_create_invoice_view.xml',

#         'views/res_partner.xml',
#         'views/account_invoice.xml',
#         'views/dmz.xml',
#         'views/sale.xml',
#         'views/product_template.xml',
#         'views/lots.xml',
#        'views/renta.xml',
#        'views/product.xml',
#        'views/menu.xml',
#        'views/stock.xml',
#        'views/purchase.xml',
#        'views/stock_picking_view.xml',
    ],
    'installable' : True,
    'application' : False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
