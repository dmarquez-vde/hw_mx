# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class hwResult(models.Model):
    _name = "hw.result"

    resultCode = fields.Char(string="Codigo")
    resultData = fields.Text(string="Data")
    nextPage = fields.Char(string="Next Page")
    message = fields.Char(string="Message")
    execution_id = fields.Many2one('hw.execution', string="Execution")
    guid = fields.Char(string="GUID")
    json = fields.Text(string="JSON")