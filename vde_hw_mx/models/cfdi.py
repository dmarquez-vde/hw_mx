# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class hwCFDI(models.Model):
    _name = "hw.cfdi"

    serie = fields.Char(string="Serie")
    folio = fields.Char(string="Folio")
    name = fields.Char(string="Factura")
    state = fields.Selection([('pending', 'En Proceso'),('success','Timbrado'),('error','Error')], string="Status")
    pdf = fields.Binary(string="PDF")
    xml = fields.Binary(string="XML")
    xml_string = fields.Text(string="XML string")
    cbb = fields.Binary(string="CBB")
    cbb_string = fields.Text(string="CBB string")
    html = fields.Binary(string="HTML")
    html_string = fields.Text(string="HTML string")
    uuid = fields.Char(string="UUID")
    date_stamp = fields.Datetime(string="Date Stamping")
    hw_no_invoice = fields.Char(string="No Invoice HW")
    hw_track_id = fields.Char(string="Track ID HW")
    hw_json = fields.Text(string="HW json")
    vde_cfdi_assoc = fields.Text(string="VDE CFDI array")
    date_entry = fields.Datetime(string="Date Entry")
    result = fields.Text(string="Result")
    date_push = fields.Datetime(string="Date Push")
    put_result_code = fields.Char(string="Put Result Code")
    put_date = fields.Datetime(string="Date Put")
    put_message = fields.Char(string="Put Message")
    is_send = fields.Boolean(string="Is Email send?")
    send_mail_account = fields.Char(string="Email account")
    send_mail_name = fields.Char(string="Email Name")
    #date_creation = fields.Datetime(string="Date Creation")
    url_pdf = fields.Char(string="URL PDF")
    url_xml = fields.Char(string="URL XML")
    sync = fields.Integer(string="Sync")
    execution_id = fields.Many2one('hw.execution', string="Execution")