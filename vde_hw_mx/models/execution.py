# -*- coding: utf-8 -*-

from odoo import api, fields, models, _

class hwExecution(models.Model):
    _name = "hw.execution"

    typeDoc = fields.Selection([(1,'Ingreso'),(2,'Egreso')], string="Tipo de documento")
    dateStart = fields.Datetime(string="Fecha Inicio")
    dateEnd = fields.Datetime(string="Fecha Fin")
    dateExecution = fields.Datetime(string="Fecha Ejecución")
    status = fields.Selection([(0,'Ok'),(1,'Error')], string="Status")
    params_crypt = fields.Text(string="Parametros Encriptados")
    params = fields.Text(string="Parametros")
    token = fields.Text(string="Token")
    cfdi_ids = fields.One2many('hw.cfdi','execution_id',string="CFDIs")